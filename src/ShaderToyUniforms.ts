namespace feng3d
{
    export class ShaderToyUniforms
    {
        iResolution = new Vector3(800, 450, 1);

        iTime = 0.0009;

        iMouse = new Vector4(0, 0, 0, 0);

        iFrame = 33;

        iChannel1 = Texture2D.default;
        iChannel2 = Texture2D.default;
        iChannel3 = Texture2D.default;
    }
}