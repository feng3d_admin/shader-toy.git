namespace feng3d
{
    export class UIUniforms
    {
        /**
         * UI几何体尺寸，在shader中进行对几何体缩放。
         */
        u_rect = new Vector4(-313.5000, -50, 627, 100);

        /** 
         * 颜色
         */
        u_color = new Color4();

        /**
         * 纹理数据
         */
        // s_texture = Texture2D.default;

        /**
         * 控制图片的显示区域。
         */
        u_uvRect = new Vector4(0, 0, 1, 1);

        u_modelMatrix = new Matrix4x4([1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 392.5000, 372, 0, 1]);

        u_viewProjection = new Matrix4x4([0.0014, 0, 0, 0, 0, -0.0027, 0, 0, 0, 0, 1, 0, -1, 1, 0, 1]);

    }

    shaderConfig.shaders["ui"] = {
        vertex: `
    attribute vec2 a_position;
    attribute vec2 a_uv;
    
    uniform vec4 u_uvRect;
    uniform vec4 u_rect;
    uniform mat4 u_modelMatrix;
    uniform mat4 u_viewProjection;
    
    varying vec2 v_uv;
    varying vec2 v_position;

    void main() 
    {
        vec2 position = u_rect.xy + a_position * u_rect.zw;
        gl_Position = u_viewProjection * u_modelMatrix * vec4(position, 0.0, 1.0);
        v_uv = u_uvRect.xy + a_uv * u_uvRect.zw;
        v_position = position.xy;
    }
    `,
        fragment: `
    precision mediump float;

    // uniform sampler2D s_texture;
    varying vec2 v_uv;
    varying vec2 v_position;
    
    uniform vec4 u_color;
    
    void main() 
    {
        // vec4 color = texture2D(s_texture, v_uv);
        vec4 color = vec4(1.0,0.0,0.0,1.0);
        gl_FragColor = color * u_color;
    }
    
    `,
        cls: UIUniforms,
        renderParams: { enableBlend: true, depthtest: false },
    };

    export function initUIMaterial()
    {
        var renderAtomic = new RenderAtomic();
        var positions = [0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0];
        var uvs = [0, 0, 1, 0, 1, 1, 0, 1];
        var indices = [0, 1, 2, 0, 2, 3];

        renderAtomic.attributes.a_position = new Attribute("a_position", positions, 3);
        renderAtomic.attributes.a_uv = new Attribute("a_uv", uvs, 2);
        renderAtomic.indexBuffer = new Index(indices);

        //
        var shaderName = "ui";
        renderAtomic.uniforms = new shaderConfig.shaders[shaderName].cls();
        renderAtomic.renderParams = shaderConfig.shaders[shaderName].renderParams;
        renderAtomic.shader = new Shader(shaderName);

        return renderAtomic;
    }
}